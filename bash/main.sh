echo "Update script successful"
echo "Would you like to redownload the script(development)

Yes or No"
 
read update

if [ $update == yes ]
then
  echo "
Redownloading the script.
"
  curl https://gitlab.com/all272/bash-web-crawler/-/raw/main/bash/main.sh --output mainupdate.sh
  echo "
Exiting script
"
  updater
fi
updater() {
  rm main.sh
  echo 'debug main.sh delete'
  mv mainupdate.sh main.sh
  echo 'debug mainupdate.sh move to main.sh'
  bash main.sh
  exit 1
  debug 'you should never see this. If you do press ctrl+c to exit the application and retry.'
}
echo "

Would you like to install any packages needed before or as they are needed?
0) before
1) as needed"
read packagesbefore
if [ $packagesbefore == before ] || [ $packagesbefore == 0 ]
then
  sudo apt update
  echo 'Installing packages'
  sudo apt -y install html2text neofetch python3 
  sudo curl -o /usr/local/bin/googler https://raw.githubusercontent.com/jarun/googler/v4.3.2/googler && sudo chmod +x /usr/local/bin/googler
  sudo googler -u
else
  echo 'Skipping package install'
fi
crawltype() {
echo 'Would you like to crawl from google search, a dictionary list, or an encyclopedia, bible, or a superlist(dictionary, encyclopedia, and bible)?
0) Google
1) Dictionary
2) Encyclopedia
3) Bible
4) Superlist' 

read crawloption }
if [ $crawloption == google ] || [ $crawloption == 0 ]
then
  if [ $packagesbefore != before ] || [ $packagesbefore != 0 ]
  then
    sudo apt -y install html2text
    sudo curl -o /usr/local/bin/googler https://raw.githubusercontent.com/jarun/googler/v4.3.2/googler && sudo chmod +x /usr/local/bin/googler
    sudo googler -u
  fi
  echo 'What do you want to search for? EXAMPLE: Python Challenge Answers'
  read googlesearch
  touch search.txt
  googler -C -n 10 $googlesearch | -a search.txt
  echo 'Would you like to search for a different term?
  
  Yes or No'
  read searchagain
  if [ $searchagain == yes ]
  then 
    searching() {
    echo 'What do you want to search for? EXAMPLE: Python Challenge Answers'
    read googlesearch
    touch search.txt
    googler -C -n 10 $googlesearch | -a search.txt
    echo 'Would you like to search for a different term?
    
    Yes or No'
    read searchagain
    }
    searching
  fi
  #curl --get https://serpapi.com/search \
  #-d q="$googlesearch" \
  #-d location="Austin%2C+Texas%2C+United+States" \
  #-d hl="en" \
  #-d gl="us" \
  #-d google_domain="google.com" \
  #-d api_key="0190e1e7af4b221d7d69111aeed63b3e199c0bef8cd5417e35a4e12550a9f7bd" --output search.txt | html2text
elif [ $crawloption = dictionary ] || [ $crawloption = 1 ]
then
  echo 'Which dictionary should I use?
  0) Oxford
  1) Merriam-Webster
  2) All'
  echo 'I have not finished this yet'
elif [ $crawloption = encyclopedia ] || [ $crawloption = 2 ]
then
  echo "Which dictionary should I use?
  0) Children's
  1) Britannica
  2) All"
  echo 'I have not finished this yet'
elif [ $crawloption = bible ] || [ $crawloption = 3 ]
then
  echo "Which bible should I use?
  0) Christain's
  1) Jewish
  2) All"
  echo 'I have not finished this yet'
elif [ $crawloption = superlist ] || [ $crawloption = 4 ]
then
  #echo 'Installing superlist'
  echo 'I have not finished this yet'
else
  echo 'Please select an actual option'
  crawltype
fi

echo "Would you like to delete all used packages?

Yes or No"

read deltepackage

if [ $deletepakage == yes ]
then
  echo "Deleting packages"
  sudo apt remove html2text
  sudo apt remove neofetch
else
  echo "It didn't matter anyway because I have no donation methods setup."
fi
echo "All code has finished executing. Exiting"
